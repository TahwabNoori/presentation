//Authors: Tahwab Noori, Carlitos Carmona, Damian Salazar
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: MyStatefulWidget(),
    );
  }
}
class LocationWidget extends StatefulWidget {
  @override
  _LocationState createState() => _LocationState();
}
class _LocationState extends State<LocationWidget> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void _showBottomSheetCallback() {
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return Container(
        decoration: BoxDecoration(
            border: Border(top: BorderSide(color: Colors.deepOrange)),
            color: Colors.orangeAccent
        ),
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: Text('Address: 123 Sesame Street, Mudug, Somalia',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 24.0,
            ),
          ),
        ),
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          key: _scaffoldKey,
          body: Center(
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.orangeAccent),
                  onPressed: _showBottomSheetCallback,
                  child: const Text('Get Our Location!')
              )
          )
    );
  }
}
class OrderWidget extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<OrderWidget> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void _showTakeoutSheetCallback() {
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return Container(
        decoration: BoxDecoration(
            border: Border(top: BorderSide(color: Colors.deepOrange)),
            color: Colors.orangeAccent
        ),
        child: Padding(
          padding: const EdgeInsets.only(
              top: 32.0, bottom: 32, left: 100, right: 100),
          child: Text('Takeout Order Placed!',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 24.0,
            ),
          ),
        ),
      );
    });
  }
  void _showDeliverySheetCallback() {
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return Container(
        decoration: BoxDecoration(
            border: Border(top: BorderSide(color: Colors.deepOrange)),
            color: Colors.orangeAccent
        ),
        child: Padding(
          padding: const EdgeInsets.only(
              top: 32.0, bottom: 32, left: 100, right: 100),
          child: Text('Delivery Order Placed!',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 24.0,
            ),
          ),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.orangeAccent),
                  onPressed: _showTakeoutSheetCallback,
                  child: const Text('Order Takeout')
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.orangeAccent),
                  onPressed: _showDeliverySheetCallback,
                  child: Text('Order Delivery')
              )
            ]
          ),
        )
    );
  }
}

/// This is the stateless widget that contains the modal sheet.
class MenuWidget extends StatelessWidget {
  MenuWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(primary: Colors.orangeAccent),
        child: const Text('See Our Menu'),
        onPressed: () {
          showModalBottomSheet<void>(
              context: context,
              builder: (BuildContext context) {
                return Container(
                  child: new Wrap(
                    children: <Widget>[
                      new ListTile(
                          leading: new Icon(Icons.whatshot_outlined),
                          title: new Text('Mild Taco'),
                          subtitle: new Text(
                              'Corn tortilla tacos served with Avocado Salsa.'),
                          onTap: () => {}),
                      new ListTile(
                        leading: new Icon(Icons.whatshot),
                        title: new Text('Spicy Burrito'),
                        subtitle: new Text(
                            'Our Iconic Burrito served with our house-made Red Chili Salsa.'),
                        onTap: () => {},
                      ),
                      new ListTile(
                        leading: new Icon(Icons.warning_amber_outlined),
                        title: new Text('Nachos del Diablo'),
                        subtitle: new Text(
                            'Fresh chips served with Carolina Reaper Diablo Salsa.\nWARNING: Extreme heat can cause hallucinations.'),
                        onTap: () => {},
                      )
                    ],
                  ),
                );
              });
        },
      ),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;

  static List<Widget> _widgetOptions = <Widget>[
    MenuWidget(),
    OrderWidget(),
    LocationWidget(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Carlitos Taqueria'),
        backgroundColor: Colors.orangeAccent,
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.local_restaurant),
            label: 'Menu',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.attach_money),
            label: 'Order',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.location_on),
            label: 'Find Us',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
